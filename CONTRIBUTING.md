# Contributing

GitBuilding is maintained by a small team, we would love more people to get involved. There are a number of ways you can get involved these include:

* Testing the code and providing feedback
* Helping with the Python or Javascript code
* Giving syntax suggestions based on how you want to write your documentation.


## Understanding version numbers

As we use semantic versioning for packaging, but we have not reached v1.0.0 stability. Major releases are currently v0.X.0. Bug-fix releases used the third number.

You can [check PyPi to see if there is a pre-release](https://pypi.org/project/gitbuilding/#history) of an upcoming version.

## Contributing to the code.

We welcome merge requests. Please look at the issues or open new ones if you want an idea of what needs doing. If you plan to do significant changes it is probably best to have a chat first, issue threads are a good place for this. You can also talk to us on [our area of the GOSH forum](https://forum.openhardware.science/c/projects/gitbuilding/55) or on [Gitter](https://gitter.im/gitbuilding/community) (though we may not be too responsive due to Matrix notification issues!)

### Installing as a developer

Please check the [developer installation guide](DeveloperInstallation.md).

### Unit tests

We use the python library `unittest` to do our unit testing. There are a number of ways to do run the same tests depending on preference

* You can run all tests by running the script `tests/test.py`.
* If you use VSCode you can Open the command palette and run `Python: Configure Tests`->`unittest`->`tests`->`test_*.py`. This will set up unit testing inside VSCode
* Run `./coverage_report.sh` this will run all tests and generate the `htmlcov` folder which graphically shows test coverage.

Once commited all tests will run on the GitLab CI on both Linux and Windows runners. The coverage stats will then be available on [codecov](https://codecov.io/gl/gitbuilding/gitbuilding).

### Linting - Python

**Linting - No autofix**  
We lint with `pylint` using a customised `.pylintrc` file which is in the `gitbuilding` directory with the python code. You can also run `lint_test.py` from this directory to lint all files.

**Auto-fixing - Please do not run `black` on this code**

While auto-fixing can solve some simple code style issues (such as basic whitespace problems) it often aggressively rearranges code to arbitrarily meet guidelines. This can harm clarity. Usually the correct approach is some manual refactoring or to define a well named variable.

Minimal auto-fixing of code issues such as whitespace can be done with

    autopep8 --in-place <filename>

But please check the result with git-diff. Paying attention to hanging indent styles.

We are developing experimental checks for code style that are not accessible in pylint. These can be run with

    ./code_style.py

if this fails it will not cause the CI pipleine to fail.

**Hanging indentation**
Code style involves some level of personal preference. GitBuilding uses the following style of hanging indentation, as it clearly separates the difference between variables in a continued statement from a true compound statements such `with`, `try`, `if` etc.


For example we use:

```python
object_with_long_name.method_with_long_name(input1,
                                            input2,
                                            input3,
                                            long_keyword=input4,
                                            long_keyword2=input5,
                                            long_keyword3=input6,
                                            long_keyword4=input7)
```

rather than:

```python
object_with_long_name.method_with_long_name(
    input1,
    input2,
    input3,
    long_keyword=input4,
    long_keyword2=input5,
    long_keyword3=input6,
    long_keyword4
)
```

###  Linting - Javascript

For javascript we use ES6lint, including the auto fix options.
