import nodeResolve from "@rollup/plugin-node-resolve";
import commonJs from "@rollup/plugin-commonjs";

export default {
  input: "main.js",
  plugins: [nodeResolve(), commonJs()],
  output: {
    format: "umd",
    file: "../../gitbuilding/static/search.js",
  },
};
