import Vue from "vue";
import AsyncComputed from "vue-async-computed";
import VueResource from "vue-resource";
import ContentsPage from "./ContentsPage.vue";
import VueSimpleAlert from "vue-simple-alert";
import Spinner from "vue-simple-spinner";
import VueModal from "@kouts/vue-modal";
import "@kouts/vue-modal/dist/vue-modal.css";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.config.productionTip = false;
Vue.use(AsyncComputed);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueResource);
Vue.use(VueSimpleAlert);
Vue.component("vue-simple-spinner", Spinner);
Vue.component("Modal", VueModal);

new Vue({
  render: h => h(ContentsPage)
}).$mount("#app");
