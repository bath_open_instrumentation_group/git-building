# Documentation for live-editor

## Installing javascript and setting up project

Install [NodeJS](https://nodejs.org/en/download/) (version 8 or higher) then run:

```
npm install
```

### Lints and fixes files

GitBuilding uses a code formatter for code style consistency. To automatically check and "fix" the code run:
```
npm run lint
```

### Build javascript for production
To build the javascript live editor for gitbuilding run:
```
npm run build
```
This should put the correct files in the static directory in GitBuilding. You will need to stop the server and reload.

### Using the Dev server

If you are working on the javascript and want the GitBuilding server to live reload any changes to javascript you should use the dev server.

To start the dev server navigate to this directory and run

```
npm run serve
```

In another terminal open you test project and run

```
gitbuilding serve --dev
```

Now when you edit any js/vue file, the webpage will auto reload every time you save.

### To customise the configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
